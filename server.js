//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

//para enviar peticion
var requestjson = require('request-json');
var urlmovimientos="https://api.mlab.com/api/1/databases/dbbanca3m906543/collections/movimientos?apiKey=84iexToCe-jR51bE3V56-iOpbAF7WZGZ";
var urlclientes ="https://api.mlab.com/api/1/databases/dbbanca3m906543/collections/usuarios?apiKey=84iexToCe-jR51bE3V56-iOpbAF7WZGZ";
var urlcuentas ="https://api.mlab.com/api/1/databases/dbbanca3m906543/collections/cuentas?apiKey=84iexToCe-jR51bE3V56-iOpbAF7WZGZ";
var movimientosMLab = requestjson.createClient(urlmovimientos);
var clientesMLab = requestjson.createClient(urlclientes);
var cuentasMLab = requestjson.createClient(urlcuentas);

//para el post
var bodyParser = require('body-parser');
app.use(bodyParser.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Request-With, Content-Type., Accept");
  next();
});


var path = require('path');

app.listen(port);

console.log('todo list RESTful API server started on: ' + port);
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// APIs RES GET
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//usuario
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
app.get('/clientes',function(req,res){
    clientesMLab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }
   });
});
app.get('/clientes/:idclientes',function(req,res){
   var unaurlclientes=arma_url(urlclientes,"CL",req.params.idclientes);
   var unaclientesMLab = requestjson.createClient(unaurlclientes);
     unaclientesMLab.get('',function(err, resM, body){
       if(err){
         console.log(err);
       }else{
         res.send(body);
       }
    });
});
app.get('/clientes/email/:myemail',function(req,res){
   var unaurlclientes=arma_url(urlclientes,"CLE",req.params.myemail);
   var unaclientesMLab = requestjson.createClient(unaurlclientes);
     unaclientesMLab.get('',function(err, resM, body){
       if(err){
         console.log(err);
       }else{

         res.send(body);
       }
    });
});
//movimientos
app.get('/movimientos',function(req,res){
    movimientosMLab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }
   });
});
//movimientos por cuenta
app.get('/movimientos/cuenta/:idcuenta',function(req,res){
   var unaurlmovimientos=arma_url(urlmovimientos,"MOC",req.params.idcuenta);
   var unamovimientosMLab = requestjson.createClient(unaurlmovimientos);
     unamovimientosMLab.get('',function(err, resM, body){
       if(err){
         console.log(err);
       }else{

         res.send(body);
       }
    });
});

app.get('/movimientos/:idcuenta&:idmovimiento',function(req,res){
   var unaurlmovimientos=arma_url(urlmovimientos,"MO",req.params.idcuenta,req.params.idmovimiento);
   var unamovimientosMLab = requestjson.createClient(unaurlmovimientos);
     unamovimientosMLab.get('',function(err, resM, body){
       if(err){
         console.log(err);
       }else{

         res.send(body);
       }
    });
});
//cuentas
app.get('/cuentas',function(req,res){
    cuentasMLab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      }else{
        res.send(body);
      }
   });
});
app.get('/cuentas/cliente/:idcliente',function(req,res){
  var unaurlcuentas=arma_url(urlcuentas,"CUCL",req.params.idcliente);
  var unacuentasMLab = requestjson.createClient(unaurlcuentas);
    unacuentasMLab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      }else{

        res.send(body);
      }
   });
});
app.get('/cuentas/:idcuenta',function(req,res){
  var unaurlcuentas=arma_url(urlcuentas,"CU",req.params.idcuenta);
  var unacuentasMLab = requestjson.createClient(unaurlcuentas);
    unacuentasMLab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      }else{

        res.send(body);
      }
   });
});
//foliador cuentas
app.get('/cuentass/foliador',function(req,res){
  var unaurlcuentas=arma_url(urlcuentas,"CU","FOLIADORCTA");
//  console.log(unaurlcuentas);
  var unacuentasMLab = requestjson.createClient(unaurlcuentas);
    unacuentasMLab.get('',function(err, resM, body){
      if(err){
        console.log(err);
      }else{

        res.send(body);
      }
   });
});
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// APIs RES POST
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//agregar registro de usuarios a mongo
app.post('/clientes',function(req,res){
   clientesMLab.post('',req.body, function(err, resM, body){
     if(err){
       console.log(err);
     }else{
       res.send(body);
     }
  });
});
//movimientos
app.post('/movimientos',function(req,res){
   movimientosMLab.post('',req.body, function(err, resM, body){
     if(err){
       console.log(err);
     }else{
       res.send(body);
     }
  });
});
//cuentas
app.post('/cuentas',function(req,res){
   cuentasMLab.post('',req.body, function(err, resM, body){
     if(err){
       console.log(err);
     }else{
       console.log("postnode:"+req.body);
       res.send(body);
     }
  });
});


//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// APIs RES PUT
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//ACTUALIZAR registro de usuarios a mongo
app.put('/clientes/:idclientes',function(req,res){
   var unaurlclientes=arma_url(urlclientes,"CL",req.params.idclientes);
   var unaclientesMLab = requestjson.createClient(unaurlclientes);
     unaclientesMLab.put('',req.body, function(err, resM, body){
       if(err){
         console.log(err);
       }else{
         res.send(body);
       }
    });
});

//ACTUALIZAR registro de cuentas a mongo
app.put('/cuentas/:idcuenta&:idmovimiento',function(req,res){
   var unaurlcuentas=arma_url(urlcuentas,"CUCL2",req.params.idcuenta,req.params.idmovimiento);
   var unacuentasMLab = requestjson.createClient(unaurlcuentas);
       //console.log(unacuentasMLab);
       unacuentasMLab.put('',req.body, function(err, resM, body){
       if(err){
         console.log(err);
       }else{
         res.send(body);
       }
    });
});

//ACTUALIZAR registro de cuentas a mongo
app.put('/cuentassact/foliador',function(req,res){
   var unaurlcuentas=arma_url(urlcuentas,"CU","FOLIADORCTA");
   var unacuentas1MLab = requestjson.createClient(unaurlcuentas);
       //console.log(unacuentas1MLab);
       unacuentas1MLab.put('',req.body, function(err, resM, body){
       if(err){
         console.log(err);
       }else{
         res.send(body);
       }
       //console.log("aqui2");
    });
});

/*
//Ejemplo: PUT http://localhost:8080/items
app.put('/usuarios', function(req, res) {
   var itemId = req.body.id;
   var data = req.body.data;
   res.send('Update ' + itemId + ' with ' + data);
});
//movimientos
app.put('/movimientos',function(req,res){
   movimientosMLab.put('',req.body, function(err, resM, body){
     if(err){
       console.log(err);
     }else{
       res.send(body);
     }
  });
});
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
// APIs RES DELETE
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//ACTUALIZAR registro de usuarios a mongo

//Ejemplo: DELETE http://localhost:8080/items
app.delete('/items/:id', function(req, res) {
   var itemId = req.params.id;
   res.send('Delete ' + itemId);
});

//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
//++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
*/

//+++++++++++
//var request = require('request');

//request.post(
    //'http://www.yoursite.com/formpage',
//    { json: { key: 'value' } },
//    function (error, response, body) {
//        if (!error && response.statusCode == 200) {
            //console.log(body)
        //}
    //}
//);
//++++++++++++++++++++++



app.get('/',function(req,res){
 //res.send('Hola mundo node1');
 res.sendFile(path.join(__dirname,'clientes.json'));
});
app.get('/Clientes/:idcliente',function(req,res){
 res.send('Aqui tiene el cliente número:'+ req.params.idcliente);
 // res.sendFile(path.join(__dirname,'clientes.json'));
});
app.post('/',function(req,res){
 res.send('Petición post recibida ahora');
});
app.put('/',function(req,res){
 res.send('Petición put recibida');
});
app.delete('/',function(req,res){
 res.send('Petición delete recibida');
});


function arma_url(mi_url,mi_opcion,mi_variable1,mi_variable2){
  var myjson= {};

  if( mi_opcion=="CU"){
    myjson.cta_idcuenta = mi_variable1;
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
  if( mi_opcion=="CUCL"){
    myjson.cta_idcliente = mi_variable1;
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
  if( mi_opcion=="CUCL2"){
    myjson.cta_idcuenta = mi_variable1;
    myjson.cta_idcliente = mi_variable2;
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
  if( mi_opcion=="CL"){
    myjson.cli_idcliente = mi_variable1;
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
  if( mi_opcion=="CLE"){
    myjson.cli_email = mi_variable1;
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
  if( mi_opcion=="MOC"){
    myjson.mov_idcuenta= mi_variable1;
    //myjson.mov_foliomov= parseInt(mi_variable2);
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
  if( mi_opcion=="MO"){
    myjson.mov_idcuenta= mi_variable1;
    myjson.mov_foliomov= parseInt(mi_variable2);
    var strmyjson = JSON.stringify(myjson);
    var consulta ="&q="+strmyjson;
    var res_url= mi_url+consulta;
  }
   return res_url;
}
